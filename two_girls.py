# -*- coding: utf-8 -*-
"""
Created on Fri Feb 24 17:09:26 2017

@author: JaneNote
"""
from pathlib import Path
import menpo.io as mio
from menpo.visualize import print_progress
from menpo.landmark import labeller, face_ibug_68_to_face_ibug_68_trimesh

path_to_lfpw = Path('C:/Users/JaneNote/Downloads/lfpw/trainset/')

image = mio.import_image(path_to_lfpw / 'image_0100.png')
image = image.crop_to_landmarks_proportion(0.2)
d = image.diagonal()
if d > 400:
    image = image.rescale(400.0 / d)
   
labeller(image, 'PTS', face_ibug_68_to_face_ibug_68_trimesh)
image.view()