# -*- coding: utf-8 -*-
"""
Created on Fri Feb 24 17:40:46 2017

@author: JaneNote
"""

import pandas
import numpy as np
from sklearn import preprocessing

points_df = pandas.read_csv("test_file.csv")

points_df_copy = points_df
X_normalized = preprocessing.normalize(np.delete(points_df.as_matrix(), len(points_df.columns) - 2, 1))
"""
print np.shape(X_normalized)

print np.shape(points_df["class"])
print points_df.columns
"""
print X_normalized


print pandas.DataFrame(np.column_stack((X_normalized, points_df["class"])), columns = points_df.columns).corr()["class"]
