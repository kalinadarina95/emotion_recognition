# -*- coding: utf-8 -*-
"""
Created on Sun May 14 16:02:33 2017

@author: JaneNote
"""
import itertools
import cv2
import pickle
import matplotlib.pyplot as plt
import path
import json
from pathlib import Path
import menpo.io as mio
from menpodetect import load_dlib_frontal_face_detector
from sklearn import metrics
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import mean_squared_error
import pandas
import emotion_recognizer as emo

recognizer = emo.EmotionRecognizer('model.pickle')
with open('fitter.pickle', 'rb') as f:
    fitter = pickle.load(f)

path_to = 'C:/Users/JaneNote/Downloads/lfpw/testset_from_webcam/hostel'
path_to_lfpw = Path(path_to)
cam = cv2.VideoCapture(0)
frame_title = "Smotri na svoe tupoe lico"
cv2.namedWindow(frame_title)
out = cv2.VideoWriter('output_miet.avi', -1, 20.0, (640,480))
img_counter = 0

while True:
    ret, frame = cam.read()

    cv2.imshow(frame_title, frame)
    if not ret:
        break
    k = cv2.waitKey(1)

    if k%256 == 27:
        # ESC pressed
        print("Escape hit, closing...")
        break
    elif k%256 == 32:
        # SPACE pressed
        img_name = "opencv_frame_{}.png".format(img_counter)
        cv2.imwrite(path_to + '/' + img_name, frame)
        image = mio.import_image(path_to_lfpw / img_name)
        image = image.as_greyscale()
      
        detect = load_dlib_frontal_face_detector()
        # Detect face
        bboxes = detect(image)
         
        if len(bboxes) > 0:
                    # Fit AAM
            result = fitter.fit_from_bb(image, bboxes[0], max_iters=[15, 5])
            print(result)

        points_name = img_name.replace('.png', '')
        pickle_name = img_name.replace('png', 'pickle')
        with open(path_to +'/' + pickle_name, 'wb') as f:
            pickle.dump(result, f)
        with open(path_to + '/points_of_' + points_name, mode='w') as f:
            json.dump(result.final_shape.tojson(), f)
        f.close()
        
        points_x = []
        points_y = []
        final_shape_points = result.final_shape.as_vector()
        for i in range(final_shape_points.size):
            if i % 2 == 0:
                points_y.append(final_shape_points[i])
            else:
                points_x.append(final_shape_points[i])
                
        for (x, y) in itertools.izip(points_x, points_y):
            cv2.circle(frame,(int(x),int(y)), 2, (0,255,0), -1)
        emotion = recognizer.predict(result)
        cv2.rectangle(frame, (335,0),(700,80),(255,255,255), cv2.FILLED)
        #cv2.rectangle(frame,(384,0),(510,128),(255,255,255), 3, thickness=cv2.FILLED)cv2.rectangle(img, (335,0),(700,80),(255,255,255), cv2.FILLED)
        k = 0
        color_r = 36
        color_g = 168
        color_b = 29
        for (emotion_name, procent) in emotion:
            if (k != 0):
                color_r = 0
                color_g = 0
                color_b = 0
            cv2.putText(frame, emotion_name + " : " + str(procent) + " %", (335, 20 + k), cv2.FONT_HERSHEY_DUPLEX, 0.6,(color_r, color_g, color_b))
            k += 20
              
        for i in range(30):
            out.write(frame)
        
        while (cv2.waitKey(1) & 0xFF != ord('q')):
             cv2.imshow(frame_title, frame) 
        print("{} written!".format(img_name))
        img_counter += 1

cam.release()
out.release()
cv2.destroyAllWindows()