# -*- coding: utf-8 -*-
"""
Created on Sun May 21 21:19:16 2017

@author: JaneNote
"""
from os import listdir, path
from sklearn import metrics
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import mean_squared_error
import pickle
import itertools
import numpy as np
import math
import pandas
from sklearn import preprocessing
import operator

class EmotionRecognizer:
    def __init__(self, model_path):
        with open (model_path) as f: 
            self.model = pickle.load(f)
        f.close()
        self.emotions = {1 : 'Calm', 2 : 'Happiness', 3 : 'Surprise'}
    
    def learn(self, trainset_path, class_file_path):
        class_file = open(class_file_path)
        files = listdir(trainset_path)
        images = filter(lambda x: x.endswith('.pts'), files)
        points_img_x = []
        points_img_y = []
        for image in images:
            k=0
            f = open(path.join(trainset_path, image))
            img_x = []
            img_y = []
            for line in f.readlines():
                if line.startswith('}'):
                    break
             
                if k < 3:
                    k += 1
                else:
                    splitted = line.split(" ")
                    img_x.append(float(splitted[0]))
                    img_y.append(float(splitted[1]))
                    
            points_img_x.append(img_x)
            points_img_y.append(img_y)
            f.close()
        learn_data = [];
        for (points_x, points_y, class_number) in itertools.izip(points_img_x, points_img_y, class_file.readlines()):
            data = self.calculate_angles_and_distances(points_x, points_y)
            data.append(int(class_number));
            learn_data.append(data)                       
        print learn_data
        return learn_data
    
    def predict(self, image):
        points_x = []
        points_y = []
        final_shape_points = image.final_shape.as_vector()
        for i in range(final_shape_points.size):
            if i % 2 == 0:
                points_y.append(final_shape_points[i])
            else:
                points_x.append(final_shape_points[i])
        angles_and_distances = self.calculate_angles_and_distances(points_x, points_y)
            
        column_names = []
        
        for i in range(len(angles_and_distances)):
            column_names.append("distance_" + str(i))
        
        test_distances_df = pandas.DataFrame([angles_and_distances], columns = column_names)
        predictions = self.model.predict(test_distances_df[column_names])
        
        procents = self.model.predict_proba(test_distances_df[column_names])[0]
        procents = [i * 100 for i in procents]
        emotion_names = self.emotions.values()
        result = zip(emotion_names, procents)
        
        #Сортируем результат по убыванию процентов
        result = sorted(result, key=operator.itemgetter(1), reverse = True)
        print result
        return result
    
    #Рассчитывает данные на основе меток изображения: расстояния и углы.
    #points - массив из координат меток в формате [y1, x1, y2, x2...].
    #Возвращает массив углов и расстояний.
    def calculate_angles_and_distances(self, points_x, points_y):
        #Расчет углов.
        points_for_angles_x = np.array([[points_x[21], points_x[22], points_x[27]], [points_x[22], points_x[42], points_x[43]],[points_x[23], points_x[44], points_x[43]], [points_x[24], points_x[25], points_x[44]],[points_x[26], points_x[44], points_x[45]],[points_x[21], points_x[38], points_x[39]],[points_x[20], points_x[37], points_x[38]], [points_x[19], points_x[18], points_x[37]], [points_x[17], points_x[36], points_x[37]],[points_x[27], points_x[28], points_x[42]], [points_x[27], points_x[39], points_x[28]],[points_x[53], points_x[54], points_x[64]], [points_x[54], points_x[64], points_x[55]], [points_x[54], points_x[64], points_x[55]], [points_x[48], points_x[60], points_x[59]], [points_x[48], points_x[60], points_x[49]]])
        points_for_angles_y = np.array([[points_y[21], points_y[22], points_y[27]], [points_y[22], points_y[42], points_y[43]],[points_y[23], points_y[44], points_y[43]], [points_y[24], points_y[25], points_y[44]],[points_y[26], points_y[44], points_y[45]],[points_y[21], points_y[38], points_y[39]],[points_y[20], points_y[37], points_y[38]], [points_y[19], points_y[18], points_y[37]], [points_y[17], points_y[36], points_y[37]],[points_y[27], points_y[28], points_y[42]], [points_y[27], points_y[39], points_y[28]],[points_y[53], points_y[54], points_y[64]], [points_y[54], points_y[64], points_y[55]], [points_y[54], points_y[64], points_y[55]], [points_y[48], points_y[60], points_y[59]], [points_y[48], points_y[60], points_y[49]]])
           
        angles = []
        for (t_x, t_y) in itertools.izip(points_for_angles_x, points_for_angles_y):
            t_x_iter = itertools.cycle(t_x)
            t_y_iter = itertools.cycle(t_y)
            for i in range(3):
                a_x = next(t_x_iter) - next(t_x_iter)
                
                b_x = next(t_x_iter) - next(t_x_iter)
                
                a_y = next(t_y_iter) - next(t_y_iter)
                
                b_y = next(t_y_iter) - next(t_y_iter)
                
                cos_alpha = (a_x * b_x + a_y * b_y) / (math.sqrt(a_x*a_x+a_y*a_y)*math.sqrt(b_x*b_x+b_y*b_y))
                
                angles.append(cos_alpha)
        
        #Расчет расстояний.
        distances = []
        
        #Приведение расстояний к одному формату.
        max_points_x = max(points_x)
        min_points_x = min(points_x)
        max_points_y = max(points_y)
        min_points_y = min(points_y)
        for i in range(0, len(points_x) - 1):
            for j in range(i+1, len(points_x) - 1):
                x_i = points_x[i]/(max_points_x - min_points_x)
                x_j = points_x[j]/(max_points_x - min_points_x)
                y_i = points_y[i]/(max_points_y - min_points_y)
                y_j = points_y[j]/(max_points_y - min_points_y)
                distance = math.sqrt((x_j- x_i)*(x_j - x_i) + (y_j - y_i)*(y_j - y_i))
                distances.append(distance)
        
        #Cбор данных в массив.
        angles_and_distances = angles + distances
        return angles_and_distances
    