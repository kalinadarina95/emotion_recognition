 #-*- coding: utf-8 -*-
"""
Created on Sun May 21 22:14:18 2017

@author: JaneNote
"""
import itertools
import pickle
import emotion_recognizer as emo
from sklearn import metrics
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import mean_squared_error
import numpy as np
import cv2
import sys
import pandas

trainset_path = 'C:/Users/JaneNote/Downloads/lfpw/testset_3em'
class_file_path = 'C:/Users/JaneNote/Downloads/lfpw/testset_3em/test_classes_3em.txt'
recognizer = emo.EmotionRecognizer('model.pickle')
#recognizer.learn(trainset_path, class_file_path)
with open('C:/Users/JaneNote/Downloads/lfpw/testset/image_0026.pickle', 'rb') as f:
    result = pickle.load(f)
emotion = recognizer.predict(result)
points_x = []
points_y = []
final_shape_points = result.final_shape.as_vector()
for i in range(final_shape_points.size):
    if i % 2 == 0:
        points_y.append(final_shape_points[i])
    else:
        points_x.append(final_shape_points[i])
                

img = cv2.imread('C:/Users/JaneNote/Downloads/lfpw/testset/image_0026.png', cv2.IMREAD_COLOR)
print emotion
#draw.text((0, 0), unicode("áéőúöüóí","utf-8"), font=font, fill=(255,255,255))
for (x, y) in itertools.izip(points_x, points_y):
    cv2.circle(img,(int(x),int(y)), 2, (0,255,0), -1)
cv2.rectangle(img, (0,400),(300,700),(255,255,255), cv2.FILLED)
cv2.rectangle(img, (335,0),(700,80),(255,255,255), cv2.FILLED)
k = 0
color_r = 36
color_g = 168
color_b = 29
for (emotion_name, procent) in emotion:
    if (k != 0):
        color_r = 0
        color_g = 0
        color_b = 0
    cv2.putText(img, emotion_name + " : " + str(procent) + " %", (0, 430 + k), cv2.FONT_HERSHEY_DUPLEX, 0.6,(color_r, color_g, color_b))
    k += 20
    

cv2.imshow('image', img)
cv2.waitKey(0)
cv2.destroyAllWindows()
