# -*- coding: utf-8 -*-
"""
Created on Sat Mar 18 18:47:59 2017

@author: JaneNote
"""

import matplotlib.pyplot as plt
import pylab
from pylab import *
import pickle

with open('fit_aam.pickle', 'rb') as f:
    result = pickle.load(f)
    
points = result.final_shape._as_vector()
points_x = []
points_y = []
for i in range(points.size):
    if i % 2 == 0:
        points_x.append(points[i])
    else:
        points_y.append(points[i])
print points_x
print points_y
plt.plot(points_x, points_y, 'ro')
for i in range(points.size / 2):
    plt.annotate(i, xy=(points_x[i], points_y[i]))
pylab.grid()
plt.show()